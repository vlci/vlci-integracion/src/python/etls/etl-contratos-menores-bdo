import xml.etree.ElementTree as ET


class ReqTemplate:

    def __init__(self,
                 xml_template: str,
                 namespaces: dict,
                 fields) -> None:
        self.tree = ET.parse(xml_template)
        self.root = self.tree.getroot()
        self.namespaces = namespaces

        if isinstance(fields, dict):
            self.fields = fields
        elif isinstance(fields, list):
            self.fields = {}
            for set in fields:
                self.fields.update(set)
        else:
            raise ValueError(
                f'fields solo puede ser un diccionario o'
                f'una lista de diccionarios')

    def __str__(self) -> str:
        return ET.tostring(self.tree.getroot(), encoding="unicode")

    def update_fields(self, **kwargs) -> None:
        for key, value in kwargs.items():
            field = self.root.find(self.fields[key], self.namespaces)
            if field is not None:
                field.text = value