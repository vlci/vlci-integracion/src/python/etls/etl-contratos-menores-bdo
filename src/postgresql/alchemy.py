from sqlalchemy import create_engine
from sqlalchemy import Engine, text
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.sql.schema import Table as SQLTable


def get_conn(host: str,
             port: str,
             database: str,
             user: str,
             password: str):
    try:
        return create_engine(
            f"postgresql://{user}:{password}@{host}:{port}/{database}")
    except Exception as e:
        raise ConnectionRefusedError(
            f"Error conectando a la base de datos: {e}")


def execute_select_distinct(engine: Engine, query: str):
    with engine.connect() as conn:
        result = conn.execute(text(query))
        valores_unicos = [row[0] for row in result]
        return valores_unicos


def upsert_df(table, conn, keys, data_iter):
    '''Esta función es un callback que usa DataFrame.to_sql'''
    insert_stmt = insert(table.table).values(list(data_iter))
    set_on_duplicate = {}
    for key in keys:
        set_on_duplicate[key] = insert_stmt.excluded[key]

    on_duplicate_key_stmt = insert_stmt.on_conflict_do_update(
        index_elements=['unidad_administrativa', 'fecha_negocio'],
        set_=set_on_duplicate
    )

    conn.execute(on_duplicate_key_stmt)
