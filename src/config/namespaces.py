areas_namespaces_request = {
    'aut': 'http://www.valencia.es/schema/Authorization',
    'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
    'wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
    'wsu': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd',
    'v30': 'http://www.valencia.es/services/esb/bdo/uo/v30',
    'con': 'http://www.valencia.es/services/mbae/bdo',
    'com': 'http://www.valencia.es/schema/mbae/Common'
}

unidad_admin_namespaces_request = {
    'aut': 'http://www.valencia.es/schema/Authorization',
    'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
    'wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
    'wsu': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd',
    'v30': 'http://www.valencia.es/services/esb/bdo/uo/v30',
    'com': 'http://www.valencia.es/schema/mbae/Common',
    'bdo': 'http://www.valencia.es/services/mbae/bdo'
}

unidad_organ_namespaces_request = {
    'aut': 'http://www.valencia.es/schema/Authorization',
    'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
    'wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
    'wsu': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd',
    'v30': 'http://www.valencia.es/services/esb/bdo/uo/v30',
    'bdo': 'http://www.valencia.es/services/mbae/bdo'
}

auth_fields = {
    'username': './/wsse:Username',
    'password': './/wsse:Password',
    'username_token': './/aut:UsernameToken'
}

all_areas_namespaces = {
    'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
    'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
    'v30': 'http://www.valencia.es/services/esb/bdo/uo/v30',
    'ns2': 'http://www.valencia.es/schema/SerTICEntidadBase',
    'ns3': 'http://www.valencia.es/services/mbae/bdo',
    'ns4': 'http://www.valencia.es/schema/mbae/Common',
    'ns5': 'http://www.valencia.es/schema/mbae/Empleado',
    'ns6': 'http://www.valencia.es/schema/mbae/Unidad',
    'ns7': 'http://www.valencia.es/schema/Authorization',
    'ns8': 'http://www.valencia.es/services/mbae/bdo/uo/v30',
    'doc': 'http://www.valencia.es/schema/SerTICFaultMessage'
}

unidad_admin_namespaces = {
    'ns3': 'http://www.valencia.es/services/mbae/bdo',
    'ns4': 'http://www.valencia.es/schema/mbae/Empleado',
    'ns5': 'http://www.valencia.es/schema/mbae/Common',
    'ns6': 'http://www.valencia.es/schema/mbae/Unidad',
    'ns7': 'http://www.valencia.es/schema/Authorization',
    'ns8': 'http://www.valencia.es/services/mbae/bdo/uo/v30',
    'v30': 'http://www.valencia.es/services/esb/bdo/uo/v30',
}

unidad_organ_namespaces = {
    'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
    'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
    'v30': 'http://www.valencia.es/services/esb/bdo/uo/v30',
    'ns2': 'http://www.valencia.es/schema/SerTICEntidadBase',
    'ns3': 'http://www.valencia.es/services/mbae/bdo',
    'ns4': 'http://www.valencia.es/schema/mbae/Common',
    'ns5': 'http://www.valencia.es/schema/mbae/Empleado',
    'ns6': 'http://www.valencia.es/schema/mbae/Unidad',
    'ns7': 'http://www.valencia.es/schema/Authorization',
    'ns8': 'http://www.valencia.es/services/mbae/bdo/uo/v30',

}

unidad_admin_fields = {
    'cod_unidad_admin': './/bdo:CodigoUnidadAdministrativa',
    'id_organigrama': './/com:IdOrganigrama'
}

unidad_orga_fields = {
    'codigo_orga_externo': './/bdo:CodigoOrganicoExterno'
}