import logging
import os
import warnings
import xml.etree.ElementTree as ET

import pandas as pd
from requests import request
from vlcishared.flow.flow import FlowControl
from vlcishared.logging.setup import log_setup

from config import config
from config.namespaces import (all_areas_namespaces, areas_namespaces_request,
                               auth_fields, unidad_admin_fields,
                               unidad_admin_namespaces,
                               unidad_admin_namespaces_request,
                               unidad_orga_fields, unidad_organ_namespaces,
                               unidad_organ_namespaces_request)
from postgresql.alchemy import execute_select_distinct, get_conn, upsert_df
from xmlparser.template import ReqTemplate

warnings.filterwarnings("ignore")


def main():
    os.makedirs(config.LOG_FOLDER, exist_ok=True)
    log_setup(config.LOG_FOLDER,
              config.ETL_NAME,
              config.LOG_LEVEL)
    log = logging.getLogger()
    db = get_conn(config.POSTGRES_HOST,
                  config.POSTGRES_PORT,
                  config.POSTGRES_DATABASE,
                  config.POSTGRES_USER,
                  config.POSTGRES_PASS)

    flow = FlowControl()
    log.info('Iniciando ejecución ETL')

    try:
        log.info('Consultando los datos en BBDD')
        unidades_admin_bbdd = execute_select_distinct(
            db,
            config.SELECT_UNIDAD_ADMIN_QUERY.format(
                TABLE=config.CONTRATOS_MENORES_TABLE))
        fecha_adjudicacion_max = execute_select_distinct(
            db,
            config.FECHA_ADJUDICACION_MAX_QUERY.format(
                TABLE=config.CONTRATOS_MENORES_TABLE))[0]
        log.info('Consultando el servicio web AllAreasRequest')
        df_all_areas_ws = all_areas_ws()
        dataset = []

        log.info('Consultando otros servicios web para crear el dataset')
        for unidad_admin in unidades_admin_bbdd:
            df_unidad_admin = find_unidad_admin_ws(unidad_admin)
            dataset_unidad = set_dataset(
                unidad_admin, df_unidad_admin, df_all_areas_ws, fecha_adjudicacion_max)

            dataset.append(dataset_unidad)

        dataframe = pd.DataFrame(dataset)
        log.info(
            'Insertando datos en la tabla t_datos_etl_unidades_administrativas_areas')
        dataframe.to_sql(name=config.UNIDADES_ADMINISTRATIVAS_TABLE,
                         schema=config.POSTGRES_SCHEMA,
                         con=db,
                         index=False,
                         if_exists='append',
                         method=upsert_df)
        return flow.end_exec()
    except Exception as e:
        flow.handle_error(
            cause=f'Fallo En la ejecución de la ETL, causa: {e}', fatal=True)


def get_primer_fila_df(identificador: str, df_unidad_admin: pd.DataFrame):
    filas = df_unidad_admin[df_unidad_admin['Identificador'] == identificador]
    return filas.iloc[0]


def get_area_data(codigo_organico_area: str, df_all_areas_ws: pd.DataFrame):
    area_data = df_all_areas_ws[df_all_areas_ws['CodigoOrganicoExterno']
                                == codigo_organico_area]
    if (area_data.empty):
        df = get_unidad_organizativa_ws(codigo_organico_area)
        area_data = df[df['CodigoOrganicoExterno'] == codigo_organico_area]
    return area_data


def get_data_not_available(unidad_admin: str, fecha_max: str):
    no_disponible = 'NO DISPONIBLE'
    area_no_disponible = {
        'unidad_administrativa': unidad_admin,
        'unidad_cod_org': no_disponible,
        'area_cod_org': no_disponible,
        'area_desc_cas': no_disponible,
        'area_desc_val': no_disponible,
        'area_desc_corta_cas': no_disponible,
        'area_desc_corta_val': no_disponible,
        'fecha_negocio': fecha_max
    }
    return area_no_disponible


def set_dataset(unidad_admin: str,
                df_unidad_admin: pd.DataFrame,
                df_all_areas_ws: pd.DataFrame,
                fecha_max: str):
    if (not df_unidad_admin.empty
            and df_unidad_admin.isin(['SECC', 'SERV']).any().any()):

        if (df_unidad_admin['Identificador'] == 'SERV').any():
            identificador = 'SERV'
        elif (df_unidad_admin['Identificador'] == 'SECC').any():
            identificador = 'SECC'
        # Tomamos los datos del primer servicio o sección.
        primer_fila = get_primer_fila_df(identificador, df_unidad_admin)
        # Se obtiene el código de área correspondiente a una unidad organizativa. Ejemplo: AK740000 es A.000000
        codigo_organico_area = (primer_fila['CodigoOrganicoExterno'])[
            0] + '.000000'
        area_data = get_area_data(
            codigo_organico_area, df_all_areas_ws)
        if area_data.empty:
            area_bdo_dataset = get_data_not_available(unidad_admin, fecha_max)
        else:
            area_bdo_dataset = {
                'unidad_administrativa': unidad_admin,
                'unidad_cod_org': primer_fila['CodigoOrganicoExterno'],
                'area_cod_org': codigo_organico_area,
                'area_desc_cas': area_data['DenominacionCas'].iloc[0],
                'area_desc_val': area_data['DenominacionVal'].iloc[0],
                'area_desc_corta_cas': area_data['DenominacionCortaCas'].iloc[0],
                'area_desc_corta_val': area_data['DenominacionCortaVal'].iloc[0],
                'fecha_negocio': fecha_max
            }
    else:
        area_bdo_dataset = get_data_not_available(unidad_admin, fecha_max)
    return area_bdo_dataset


def get_soap_config(req_model: str, req_namespaces: dict, specific_fields=None, upd_fields=None):
    soap_config = {
        'req_model': req_model,
        'req_namespaces': req_namespaces,
        'fields': [auth_fields],
        'update_fields': {
            'username': config.SOAP_SERVICE_USER,
            'password': config.SOAP_SERVICE_PASSWORD,
            'username_token': config.SOAP_SERVICE_USER
        },
        'service_url': config.BDO_SERVICE_URL
    }
    if specific_fields:
        soap_config['fields'].append(specific_fields)
    if upd_fields:
        soap_config['update_fields'].update(upd_fields)

    return soap_config


def read_xml_area_data(unidad_organ):
    unidad_data = {}
    codigo_organico_externo = unidad_organ.find(
        './ns4:CodigoOrganicoExterno', all_areas_namespaces)
    adscripciones = unidad_organ.find(
        './/ns6:Adscripciones', all_areas_namespaces)
    if adscripciones is not None:
        adscripcion = adscripciones.find(
            './/ns6:Adscripcion', all_areas_namespaces)
        if adscripcion is not None:
            elementos = [
                ('DenominacionCas', ["ÁREA DE"]),
                ('DenominacionVal', ["ÀREA D’", "ÀREA DE"]),
                ('DenominacionCortaCas', ["ÁREA DE"]),
                ('DenominacionCortaVal', ["ÀREA D’", "ÀREA DE"])
            ]
            for element_name, prefixes in elementos:
                element = adscripcion.find(
                    f'.//ns6:{element_name}', all_areas_namespaces)
                for prefix in prefixes:
                    unidad_data[element_name] = element.text.replace(
                        prefix, "").strip() if element is not None else None
    if codigo_organico_externo is not None:
        unidad_data['CodigoOrganicoExterno'] = codigo_organico_externo.text

    return unidad_data


def find_unidad_admin_ws(codigo: str):
    '''Web service que nos devuelve todas las unidades organizativas
    existentes en la BDO para cierta unidad administrativa.'''
    update_fields = {
        'cod_unidad_admin': codigo,
        'id_organigrama': 'FUNCIONAL'
    }
    unidad_admin_config = get_soap_config(
        req_model=config.XML_FIND_UNIDAD_ADMIN_MODEL,
        req_namespaces=unidad_admin_namespaces_request,
        specific_fields=unidad_admin_fields,
        upd_fields=update_fields)

    unidad_admin_response_xml = get_data_from_soap(unidad_admin_config)
    root = ET.fromstring(unidad_admin_response_xml)
    data = []
    unidades_organizativas = root.findall(
        './/ns6:UnidadOrganizativa', unidad_admin_namespaces)
    if unidades_organizativas:
        for unidad in unidades_organizativas:
            unidad_data = {}
            codigo_orga_externo = unidad.find(
                './/ns5:CodigoOrganicoExterno', unidad_admin_namespaces)
            unidad_data['CodigoOrganicoExterno'] =\
                codigo_orga_externo.text if codigo_orga_externo is not None else None
            tipo = unidad.find('.//ns5:Tipo', unidad_admin_namespaces)
            if tipo is not None:
                identificador = tipo.find(
                    './/ns5:Identificador', unidad_admin_namespaces)
                unidad_data['Identificador'] =\
                    identificador.text if identificador is not None else None
            data.append(unidad_data)
    return pd.DataFrame(data)


def all_areas_ws():
    '''Web service que devuelve varias unidades organizativas.
    Cada una de estas unidades es un área de la BDO'''
    all_areas_config = get_soap_config(
        req_model=config.XML_ALL_AREAS_MODEL,
        req_namespaces=areas_namespaces_request)
    all_areas_response_xml = get_data_from_soap(all_areas_config)
    root = ET.fromstring(all_areas_response_xml)
    data = []
    for unidad in root.findall('.//ns6:UnidadOrganizativa',
                               all_areas_namespaces):
        unidad_data = read_xml_area_data(unidad)
        data.append(unidad_data)
    return pd.DataFrame(data)


def get_unidad_organizativa_ws(codigo: str):
    '''Web service que nos devuelve información de una unidad organizativa
    según su código orgánico externo.'''
    update_fields = {
        'codigo_orga_externo': codigo
    }
    unidad_orga_config = get_soap_config(
        req_model=config.XML_GET_UNIDAD_ORG_MODEL,
        req_namespaces=unidad_organ_namespaces_request,
        specific_fields=unidad_orga_fields,
        upd_fields=update_fields)

    unidad_organ_response_xml = get_data_from_soap(unidad_orga_config)
    root = ET.fromstring(unidad_organ_response_xml)
    data = []
    for unidad in root.findall('.//ns3:UnidadOrganizativa',
                               unidad_organ_namespaces):
        unidad_data = read_xml_area_data(unidad)
        data.append(unidad_data)
    return pd.DataFrame(data)


def get_data_from_soap(config: dict) -> str:
    areas_body = ReqTemplate(
        config['req_model'],
        config['req_namespaces'],
        config['fields']
    )

    areas_body.update_fields(
        **config['update_fields']
    )

    result = request('POST',
                     config['service_url'],
                     data=str(areas_body),
                     verify=False,
                     timeout=3)

    if result.status_code != 200:
        raise ConnectionError(
            f'Fallo en la consulta al web service: {result.reason}')
    return result.text


if __name__ == '__main__':
    main()
