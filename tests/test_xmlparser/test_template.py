import unittest
import xml.etree.ElementTree as ET
from unittest.mock import patch

from src.xmlparser.template import ReqTemplate


class TestReqTemplate(unittest.TestCase):

    def setUp(self):
        # Sample XML template
        self.xml_template = (
            'tests/test_xmlparser/files/test_req_template.xml'
        )
        # Sample namespaces
        self.namespaces = {"ns1": "namespace1"}

    def test_init_with_dict_fields(self):
        fields = {
            "field1": ".//ns1:field1",
            "field2": ".//ns1:field2"
        }
        req_template = ReqTemplate(self.xml_template, self.namespaces, fields)
        self.assertIsInstance(req_template, ReqTemplate)

    def test_init_with_list_fields(self):
        fields = [
            {
                "field1": ".//ns1:field1"
            },
            {
                "field2": ".//ns1:field2"
            }
        ]
        req_template = ReqTemplate(self.xml_template, self.namespaces, fields)
        self.assertIsInstance(req_template, ReqTemplate)

    def test_init_invalid_fields_type(self):
        with self.assertRaises(ValueError):
            fields = "Invalid"
            ReqTemplate(self.xml_template, self.namespaces, fields)

    def test_str(self):
        fields = {
            "field1": ".//ns1:field1",
            "field2": ".//ns1:field2"
        }
        req_template = ReqTemplate(self.xml_template, self.namespaces, fields)

        expected_result = """
        <root xmlns:ns0="namespace1">
            <ns0:field1>Value1</ns0:field1>
            <ns0:field2>Value2</ns0:field2>
        </root>
        """.replace(" ", "").replace("\n", "")

        result = str(req_template).replace(" ", "").replace("\n", "")
        self.assertEqual(result, expected_result)

    def test_update_fields(self):

        fields = {
            "field1": ".//ns1:field1",
            "field2": ".//ns1:field2"
        }
        req_template = ReqTemplate(self.xml_template, self.namespaces, fields)
        req_template.update_fields(field1="NewValue1", field2="NewValue2")

        expected_result = """
        <root xmlns:ns0="namespace1">
            <ns0:field1>NewValue1</ns0:field1>
            <ns0:field2>NewValue2</ns0:field2>
        </root>
        """.replace(" ", "").replace("\n", "")

        result = str(req_template).replace(" ", "").replace("\n", "")
        self.assertEqual(result, expected_result)


if __name__ == '__main__':
    unittest.main()
